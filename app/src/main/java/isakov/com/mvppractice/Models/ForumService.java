package isakov.com.mvppractice.Models;

import java.util.ArrayList;
import java.util.HashMap;

import isakov.com.mvppractice.Utils.Constants;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Query;

/**
 * Created by Isakov on 22-Sep-17.
 */

public interface ForumService {

    @GET("/api/resources/albums")
    Call<ArrayList<AlbumModel>> getAlbumsSample(@Query("order") String order,
                                                @Query("genre") String genre_id,
                                                @Query("offset") String offset,
                                                @Query("limit") String limit);

    @PUT(Constants.REGISTER_URL)
    Call<ResponseBody> postRegister(@Body HashMap<String, String> hashMap);

}

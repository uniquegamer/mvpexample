package isakov.com.mvppractice.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Isakov on 22-Sep-17.
 */

    public class AlbumModel implements Parcelable {
        private ArtistModel artist;
        private String coverURL;
        private GenreModel genre;
        private int id;
        private String name;
        private String releaseDate;
        private String imagePath;

        public AlbumModel() {

        }

        public String getImagePath() {
            return imagePath;
        }

        public void setImagePath(String imagePath) {
            this.imagePath = imagePath;
        }

        public ArtistModel getArtist() {
            return artist;
        }

        public void setArtist(ArtistModel artist) {
            this.artist = artist;
        }

        public GenreModel getGenre() {
            return genre;
        }

        public void setGenre(GenreModel genre) {
            this.genre = genre;
        }

        public String getCoverURL() {
            return coverURL;
        }

        public void setCoverURL(String coverURL) {
            this.coverURL = coverURL;
        }

        public String getAlbum() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getReleaseDate() {
            return releaseDate;
        }

        public void setReleaseDate(String releaseDate) {
            this.releaseDate = releaseDate;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        protected AlbumModel(Parcel in) {
            artist = (ArtistModel) in.readValue(ArtistModel.class.getClassLoader());
            coverURL = in.readString();
            genre = (GenreModel) in.readValue(GenreModel.class.getClassLoader());
            id = in.readInt();
            name = in.readString();
            releaseDate = in.readString();
            imagePath = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(artist);
            dest.writeString(coverURL);
            dest.writeValue(genre);
            dest.writeInt(id);
            dest.writeString(name);
            dest.writeString(releaseDate);
            dest.writeString(imagePath);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<AlbumModel> CREATOR = new Parcelable.Creator<AlbumModel>() {
            @Override
            public AlbumModel createFromParcel(Parcel in) {
                return new AlbumModel(in);
            }

            @Override
            public AlbumModel[] newArray(int size) {
                return new AlbumModel[size];
            }
        };
}

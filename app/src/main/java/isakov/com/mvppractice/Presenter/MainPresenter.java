package isakov.com.mvppractice.Presenter;

import java.util.HashMap;

import isakov.com.mvppractice.Models.ForumService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Isakov on 22-Sep-17.
 */

public class MainPresenter implements MainMVP.Presenter {

    private ForumService service;
    private final MainMVP.View view;

    public MainPresenter(ForumService service, MainMVP.View view) {
        this.service = service;
        this.view = view;
    }

    @Override
    public void load(HashMap<String, String> hashMap) {
        Call<ResponseBody> call = service.postRegister(hashMap);
        view.loading();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    view.success(response.message());
                }
                view.stopLoading();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                view.error(t.getMessage());
                view.stopLoading();
            }
        });
    }

}

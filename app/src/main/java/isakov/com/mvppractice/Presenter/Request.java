package isakov.com.mvppractice.Presenter;

/**
 * Created by Isakov on 22-Sep-17.
 */

public interface Request {

    void loading();

    void stopLoading();
}

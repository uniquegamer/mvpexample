package isakov.com.mvppractice.Presenter;

import java.util.HashMap;

/**
 * Created by Isakov on 22-Sep-17.
 */

public interface MainMVP {

    interface View extends Request {

        void success(String msg);

        void error(String msg);

    }

    interface Presenter {

        void load(HashMap<String, String> hashMap);

    }
}

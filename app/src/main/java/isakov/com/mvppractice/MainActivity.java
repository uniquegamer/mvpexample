package isakov.com.mvppractice;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import isakov.com.mvppractice.Presenter.MainMVP;
import isakov.com.mvppractice.Presenter.MainPresenter;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, MainMVP.View {

    private MainPresenter presenter;
    private StartApplication app;
    private ProgressDialog dialog;
    @BindView(R.id.button)
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

        button.setOnClickListener(this);
    }

    private void init() {
        app = (StartApplication) getApplicationContext();
        ButterKnife.bind(this);
        dialog = new ProgressDialog(this);
        dialog.setMessage("Please wait...");
        presenter = new MainPresenter(app.getService(), this);
    }

    @Override
    public void onClick(View view) {
        HashMap<String, String> hash = new HashMap<>();
        hash.put("phone", "996553434333");
        presenter.load(hash);
    }

    @Override
    public void success(String msg) {
        Toast.makeText(this, "SUCCEESS" + msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void error(String msg) {
        Toast.makeText(this, "ERROR: " + msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loading() {
        dialog.show();
    }

    @Override
    public void stopLoading() {
        dialog.cancel();
    }
}

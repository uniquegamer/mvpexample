package isakov.com.mvppractice;

import android.app.Application;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import isakov.com.mvppractice.Models.ForumService;
import isakov.com.mvppractice.Utils.Constants;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Isakov on 22-Sep-17.
 */

public class StartApplication extends Application {
    private ForumService service;
    String userHash = null;

    @Override
    public void onCreate() {
        super.onCreate();
        service = createService(userHash);

    }

    public ForumService getService() {
        return service;
    }

    private ForumService createService(String userHash) {
        return new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(getClient(userHash))
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(ForumService.class);

    }

    private OkHttpClient getClient(final String u_hash) {
        return new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
                        Request.Builder ongoing = chain
                                .request().newBuilder()
                                .addHeader("Accept", "application/json;versions=1");
                        if (u_hash != null)
                            ongoing.addHeader("User-Hash", u_hash);
                        return chain.proceed(ongoing.build());
                    }
                })//.retryOnConnectionFailure(false)
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .build();
    }
}
